from .keys import PEXELS_API_KEY as PXKEY, OPEN_WEATHER_API_KEY as WXKEY
import requests
import json


def get_location_image(city, state):
    headers = {"Authorization": PXKEY}
    url = f"https://api.pexels.com/v1/search?query={city}&{state}"
    response = requests.get(url, headers=headers)
    content = json.loads(response.content)
    location_image = {
        "image": content["photos"][0]["src"]["original"],
    }
    return location_image


def get_weather_data(city, state):
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&appid={WXKEY}"
    geo_response = requests.get(geo_url)
    parsed_response = json.loads(geo_response.content)
    try:
        lat_lon = {
            "lat": parsed_response[0]["lat"],
            "lon": parsed_response[0]["lon"],
        }
    except (KeyError, IndexError):
        return None

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat_lon['lat']}&lon={lat_lon['lon']}&units=imperial&appid={WXKEY}"
    weather_response = requests.get(weather_url)
    parsed_weather_response = json.loads(weather_response.content)
    try:
        current_weather = {
            "temp": parsed_weather_response["main"]["temp"],
            "description": parsed_weather_response["weather"][0]["description"],
        }
        return current_weather
    except (KeyError, IndexError):
        return None
